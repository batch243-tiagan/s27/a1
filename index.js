// Users
{
    "id": 1,
    "firstName": "John",
    "lastName": "Doe",
    "email": "johndoe@mail.com",
    "password": "john1234",
    "isAdmin": false,
    "mobileNo": "09237593671"
}

{
    "id": 2,
    "firstName": "Hernan Jay",
    "lastName": "Tiagan",
    "email": "itsmehernanjay@gmail.com",
    "password": "test123",
    "isAdmin": false,
    "mobileNo": "091234567"
}


// Orders
{
    "id": 10,
    "userId": 1,
    "productID" : 25,
    "transactionDate": "08-15-2021",
    "status": "paid",
    "total": 1500
}

{
    "id": 11,
    "userId": 2,
    "productID" : 26,
    "transactionDate": "08-15-2021",
    "status": "paid",
    "total": 50000
}

// Products
{
    "id": 25,
    "name": "Humidifier",
    "description": "Make your home smell fresh any time.",
    "price": 300,
    "stocks": 1286,
    "isActive": true,
}

{
    "id": 26,
    "name": "Bucker",
    "description": "It's a bucket",
    "price": 50,
    "stocks": 5000,
    "isActive": true,
}

